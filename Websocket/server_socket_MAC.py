import socket
import threading

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
MAC = 'b8:86:87:bb:c8:1d' #mac address

server.bind(ADDR) #apunta a address IPv4

def handle_client(conn, addr): #maneja nuestras conexiones
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg == DISCONNECT_MESSAGE:
                connected = False

            print(f"[{addr}] {msg}")
            conn.send("Msg received".encode(FORMAT))

    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        msg_length = conn.recv(HEADER).decode(FORMAT)
        #Handshake
        if msg_length:
            msg_length = int(msg_length)
            mac_rec = conn.recv(msg_length).decode(FORMAT)
            if mac_rec == MAC:
                conn.send("Valid Authentication".encode(FORMAT))
                thread = threading.Thread(target=handle_client, args=(conn, addr))
                thread.start()
                print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")
        
            else: 
                print("Invalid Authentication")
                conn.send("Invalid Authentication".encode(FORMAT))
                conn.close()
                break

print("[STARTING] server is running.....")
start()



