#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

//declaracion de funciones
float getavg (float arr[], int size);
float getvar (float arr[], float prom, int size);
//float desvest();
float getMode(float arr[], int size);


int main() {

int i;
float ALU[100];
float media;
float varianza;
float desvest;
float moda;

srand(time(NULL));

for (i=0;i<100;i++){

    float nota = rand()%(7)+1;

    ALU[i]=nota;
    //printf("nota%d %5.1f\n", i, ALU[i]);              //permite imprimir las notas
    }

    media = getavg(ALU, 100);               //Calcula la media
    varianza = getvar(ALU, media, 100);     //Calcula la varianza
    desvest = sqrt(varianza);               //Calcula la desviación estandar
    moda = getMode(ALU, 100);               //Calcula la moda

    printf("La media es %5.2f\n", media);
    printf("La varianza es %5.3f\n", varianza);
    printf("La desviación estandar es %5.3f\n", desvest);
    printf("La moda es %5.1f\n", (moda+1));

return 0;
}

// Funciones

float getavg(float arr[], int size){
    float med;
    int i;
    float sum = 0;

    for (int i = 0; i < 100; ++i){
    sum += arr[i];
    }

    med = sum/size;

  return med;
}

float getvar(float arr[], float prom, int size){
    float vari;
    int i;
    float sum;

    for (int i = 0; i < 100; ++i) {

        sum = sum + (arr[i]*arr[i]);
    }

    vari = (sum/size) - (prom*prom);

  return vari;

}


float getMode(float arr[], int size) {

int i, j;
int frec[7]={1, 2, 3, 4, 5, 6, 7};
int frecu=0;
int mayor=0;
float moda;

for(j=0;j<7;j++){                       //Genera la tabla de frecuencias de las notas
        for(i=0;i<100;i++){
            if (frec[j]==arr[i])
            {
                frecu = frecu + 1;
            }
            else {
                frecu=frecu;
            }
        }

        frec[j]=frecu;
        frecu=0;

    }


    for(i=0;i<7;i++){               //Ubica la frecuencia más alta en la tabla modal
        if (frec[i]>mayor)
        {
            moda=i;
            mayor=frec[i];
        }

    }


    return moda;


}
