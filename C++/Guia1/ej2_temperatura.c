#include <stdio.h>


int main() {
    int s;
    float C;
    float F;

    printf("Introduzca el número de sonidos emitidos por la chicharra:\n");
    scanf("%d", &s);

    F=(s/4)+40;
    C=(0.55555555)*(F-32);

    printf("La Temperatura en grados Celcius es de %f °C \n", C );

    return 0;

}
