#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void promedio(float arr[]);
void maxima(float arr[]);
void minima(float arr[]);

//Variable globales
float Total=0.0;
float promtem=0.0;
float maxitem=-10;
float minitem=50;
int hmax=0;
int hmin=0;


int main()
{
int i;
float arreglo[24];

    //Generación de temperaturas
    srand(time(NULL));

    for(i=0;i<24;i++){
    arreglo[i] = rand()%41;
    //printf("%f\n", arreglo[i]); //permite imprimir las temperaturas del día
    }

    promedio(arreglo);
    maxima(arreglo);
    minima(arreglo);

    printf("El promedio de las temperaturas fue: %5.2f °C \n", promtem);
    printf("La temperatura máxima fue: %5.1f °C y ocurrió a las %d horas \n", maxitem, hmax);
    printf("La temperatura mínima fue: %5.1f °C y ocurrió a las %d horas \n", minitem, hmin);


    return 0;
}

void promedio(float arr[]){
int i;

    for(i=0;i<24;i++){
        Total+=arr[i];
    }

    promtem=Total/24;

}

void maxima(float arr[]){
int i;

    for(i=0;i<24;i++){
        if(arr[i]>maxitem){
            maxitem = arr[i];
            hmax=i;
        }

        }

    }

void minima(float arr[]){
int i;

    for(i=0;i<24;i++){
        if(arr[i]<minitem){
            minitem = arr[i];
            hmin=i;
        }

        }

    }

