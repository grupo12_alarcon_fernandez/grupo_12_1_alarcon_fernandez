from decimal import *
import math

#Fija la precisión en 500 decimales
n = 500
getcontext().prec = n

def chudnosky(n):
    pi = Decimal(0)
    C=426880*Decimal(10005).sqrt()
    suma = 0
    k=0


    while k<n:
        m_k = Decimal(math.factorial(6*k))/(Decimal(math.factorial(3*k))*Decimal((math.factorial(k)**3)))
        l_k = (545140134*k) + 13591409
        x_k = (-262537412640768000)**k
        suma += (m_k*l_k)/x_k
        k += 1

    pi = C/suma
    return pi


print("El valor de pi con", n, "decimales es:", "\n \n", chudnosky(1))

